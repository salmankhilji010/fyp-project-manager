﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FYP_project_manager
{
    /// <summary>
    /// Interaction logic for AddAdvisor.xaml
    /// </summary>
    public partial class AddAdvisor : UserControl
    {
        public int count = 0, count1 = 0;
        public AddAdvisor()
        {
            InitializeComponent();
        }

        private void ComboBox1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (count == 0)
            {
                count++;
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select Value from lookup where Category = 'GENDER'", con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string item = reader.GetString(0);
                    this.ComboBox1.Items.Add(item);
                    Console.WriteLine(item);
                }

                reader.Close();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string gender = "";
                string regno = this.RegNo.Text;
                string fname = this.FName.Text;
                string lname = this.LName.Text;
                string email = this.Email.Text;
                string contact = this.Contact.Text;
                string temp = this.salary.Text;
                double salary = double.Parse(temp);
                DateTime date = (DateTime)this.date.SelectedDate.Value;
                string d = date.ToString("yyyy/MM/dd");
                if (this.ComboBox1.SelectedIndex == 0)
                {
                    gender = "Male";
                }
                else
                {
                    gender = "Female";
                }
                if (regno == "" || fname == "" || lname == "" || email == "" || contact == "" || d == null || gender == null)
                {
                    MessageBox.Show("Please Enter data in all the fields.");
                    return;
                }
                else
                {
                    int i = 0; bool flag = true;
                    while (i < email.Length)
                    {

                        if (email[i] == '@')
                        {
                            flag = false;
                        }
                        i++;
                    }
                    if (flag == true)
                    {
                        MessageBox.Show("Enter a valid Email Address");
                        return;
                    }
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Select ID from lookup where Value = '" + gender + "'", con);
                    int ID = (int)cmd.ExecuteScalar();

                    SqlCommand cmd1 = new SqlCommand("Select ID from lookup where Value = '" + this.ComboBox2.SelectedValue + "'", con);
                    int desig = (int)cmd1.ExecuteScalar();

                    string query = "insert into Person(FirstName, LastName, Contact, Email, DateOfBirth, Gender) Values('" + fname + "','" + lname + "','" + contact + "','" + email + "','" + d + "'," + ID + ")";
                    var con1 = Configuration.getInstance().getConnection();
                    SqlCommand command = new SqlCommand(query, con1);
                    command.ExecuteNonQuery();

                    string query2 = "Select ID from Person where FirstName = '" + fname + "' And LastName = '" + lname + "' AND Contact = '" + contact + "' AND email = '" + email + "'";
                    var connection = Configuration.getInstance().getConnection();
                    SqlCommand command2 = new SqlCommand(query2, connection);
                    int ID1 = (int)command2.ExecuteScalar();

                    string query1 = "insert into Advisor(ID, Designation, Salary) Values(" + ID1 + ", " + desig + " , " + salary + ")";
                    var con2 = Configuration.getInstance().getConnection();
                    SqlCommand command1 = new SqlCommand(query1, con2);
                    command1.ExecuteNonQuery();

                    MessageBox.Show("Record has been inserted");
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.ToString());
            }
        }

        private void NumericTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsNumeric(e.Text);

        }
        private bool IsNumeric(string input)
        {
            return int.TryParse(input, out _);
        }

        private void ComboBox2_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (count1 == 0)
            {
                count1++;
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select Value from lookup where Category = 'DESIGNATION'", con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string item = reader.GetString(0);
                    this.ComboBox2.Items.Add(item);
                    Console.WriteLine(item);
                }

                reader.Close();
            }
        }
    }
}
