﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FYP_project_manager
{
    /// <summary>
    /// Interaction logic for AddEval.xaml
    /// </summary>
    public partial class AddEval : UserControl
    {
        public AddEval()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int ID = 0, ID1 = 0;
                string name = this.name.Text;
                int marks = int.Parse(this.Marks.Text);
                int weightage = int.Parse(this.Weightage.Text);
                var con = Configuration.getInstance().getConnection();
                SqlCommand command = new SqlCommand("insert into [Evaluation] Values('" + name + "'," + marks + "," + weightage + ")", con);
                command.ExecuteNonQuery();
                SqlCommand cmd1 = new SqlCommand("Select * from Evaluation", con);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd1);
                DataTable tb = new DataTable();
                adapter.Fill(tb);
                this.dataGrid.ItemsSource = tb.DefaultView;


            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.ToString());
            }
        }
    }
}
