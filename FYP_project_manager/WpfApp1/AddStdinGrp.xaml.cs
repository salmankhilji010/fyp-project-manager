﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FYP_project_manager
{
    /// <summary>
    /// Interaction logic for AddStdinGrp.xaml
    /// </summary>
    public partial class AddStdinGrp : UserControl
    {
        public int count = 0;
        public AddStdinGrp()
        {
            InitializeComponent();
        }

        private void ComboBox1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (count == 0)
            {
                count++;
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select Value from lookup where Category = 'STATUS'", con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string item = reader.GetString(0);
                    this.ComboBox1.Items.Add(item);
                    Console.WriteLine(item);
                }

                reader.Close();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int ID = 0, sts = 0;
                int groupID = int.Parse(this.GroupID.Text);
                int studentID = int.Parse(this.StdID.Text);
                string status = this.ComboBox1.SelectedValue.ToString();
                DateTime currentDateTime = DateTime.Now;
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select ID from [Group] where ID = '" + groupID + "'", con);
                ID = (int)cmd.ExecuteScalar();
                SqlCommand cmd2 = new SqlCommand("Select ID from lookup where Value = '" + status + "'", con);
                sts = (int)cmd2.ExecuteScalar();
                if (ID == groupID)
                {
                    SqlCommand cmd1 = new SqlCommand("Select ID from Student where ID = '" + studentID + "'", con);
                    int ID1 = (int)cmd1.ExecuteScalar();
                    if (ID1 == studentID)
                    {
                        using (SqlCommand command = new SqlCommand("insert into [GroupStudent] Values(" + groupID + "," + studentID + "," + sts + "," + "@Date)", con))
                        {
                            command.Parameters.AddWithValue("@Date", currentDateTime);
                            command.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid Student ID");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid Group Id");
                }
            }
            catch(Exception e1)
            {
                MessageBox.Show(e1.ToString());
            }
            
        }

        private void ComboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
