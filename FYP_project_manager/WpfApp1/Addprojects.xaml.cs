﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FYP_project_manager
{
    /// <summary>
    /// Interaction logic for Addprojects.xaml
    /// </summary>
    public partial class Addprojects : UserControl
    {
        public Addprojects()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime currentDateTime = DateTime.Now;
            var con = Configuration.getInstance().getConnection();
            using (SqlCommand command = new SqlCommand("insert into [Project] Values(@Discription, @title)", con))
            {
                command.Parameters.AddWithValue("@Discription", this.Desc.Text);
                command.Parameters.AddWithValue("@title", this.title.Text);
                command.ExecuteNonQuery();
            }
            SqlCommand cmd1 = new SqlCommand("Select * from [Project]", con);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            this.dataGrid.ItemsSource = dt.DefaultView;

            this.Desc.Clear();
            this.title.Clear();
        }
    }
}
