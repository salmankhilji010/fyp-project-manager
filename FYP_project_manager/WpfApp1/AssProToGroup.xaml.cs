﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FYP_project_manager
{
    /// <summary>
    /// Interaction logic for AssProToGroup.xaml
    /// </summary>
    public partial class AssProToGroup : UserControl
    {
        public AssProToGroup()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int ID = 0, ID1 = 0;
                int GroupID = int.Parse(this.GroupID.Text);
                int ProjectID = int.Parse(this.PID.Text);
                DateTime currentDateTime = DateTime.Now;
                var con = Configuration.getInstance().getConnection();


                using (SqlCommand command = new SqlCommand("insert into [GroupProject] Values(" + ProjectID + "," + GroupID + "," + "@Date)", con))
                {
                    command.Parameters.AddWithValue("@Date", currentDateTime);
                    command.ExecuteNonQuery();
                }
                SqlCommand cmd1 = new SqlCommand("Select * from GroupProject", con);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd1);
                DataTable tb = new DataTable();
                adapter.Fill(tb);
                this.dataGrid.ItemsSource = tb.DefaultView;


            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.ToString());
            }
        }
    }
}
