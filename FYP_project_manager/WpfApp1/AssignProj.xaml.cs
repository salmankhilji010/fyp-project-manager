﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FYP_project_manager
{
    /// <summary>
    /// Interaction logic for AssignProj.xaml
    /// </summary>
    public partial class AssignProj : UserControl
    {
        public int count = 0;
        public AssignProj()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int ID = 0, ID1 = 0;
                int AdvisorID = int.Parse(this.AID.Text);
                int ProjectID = int.Parse(this.PID.Text);
                string role = this.ComboBox1.SelectedValue.ToString();
                DateTime currentDateTime = DateTime.Now;
                var con = Configuration.getInstance().getConnection();
                
                SqlCommand cmd2 = new SqlCommand("Select ID from lookup where Value = '" + role + "'", con);
                ID1 = (int)cmd2.ExecuteScalar();
                
                using (SqlCommand command = new SqlCommand("insert into [ProjectAdvisor] Values(" + AdvisorID + "," + ProjectID + "," + ID1 + "," + "@Date)", con))
                {
                    command.Parameters.AddWithValue("@Date", currentDateTime);
                    command.ExecuteNonQuery();
                }
                SqlCommand cmd1 = new SqlCommand("Select * from ProjectAdvisor",con);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd1);
                DataTable tb = new DataTable();
                adapter.Fill(tb);
                this.dataGrid.ItemsSource = tb.DefaultView;
                

            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.ToString());
            }

        }

        private void ComboBox1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (count == 0)
            {
                count++;
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select Value from lookup where Category = 'ADVISOR_ROLE'", con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string item = reader.GetString(0);
                    this.ComboBox1.Items.Add(item);
                    Console.WriteLine(item);
                }

                reader.Close();
            }
        }

        private void ComboBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
