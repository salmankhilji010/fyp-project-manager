﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FYP_project_manager
{
    /// <summary>
    /// Interaction logic for Group.xaml
    /// </summary>
    public partial class Group : UserControl
    {
        public Group()
        {
            InitializeComponent();
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            this.addstd.Visibility = Visibility.Collapsed;
            this.dataGrid.Visibility = Visibility.Visible;
            DateTime currentDateTime = DateTime.Now; 
            var con = Configuration.getInstance().getConnection();
            using (SqlCommand command = new SqlCommand("insert into [Group] Values(@Date)", con))
            {
                command.Parameters.AddWithValue("@Date", currentDateTime);
                command.ExecuteNonQuery();
            }
            SqlCommand cmd1 = new SqlCommand("Select * from [Group]", con);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            this.dataGrid.ItemsSource = dt.DefaultView;

        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            this.dataGrid.Visibility = Visibility.Collapsed;
            this.addstd.Visibility = Visibility.Visible;
        }

        private void RadioButton_Checked_2(object sender, RoutedEventArgs e)
        {
            this.addstd.Visibility = Visibility.Collapsed;
            this.dataGrid.Visibility = Visibility.Visible;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Select * from [Group]", con);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            this.dataGrid.ItemsSource = dt.DefaultView;
        }

        private void RadioButton_Checked_3(object sender, RoutedEventArgs e)
        {
            this.addstd.Visibility = Visibility.Collapsed;
            this.dataGrid.Visibility = Visibility.Visible;
            var con = Configuration.getInstance().getConnection();
            string query = "Select * from [GroupStudent]";
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            this.dataGrid.ItemsSource = dt.DefaultView;
        }
    }
}
