﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Runtime;
using System.Windows.Interop;

namespace FYP_project_manager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        [DllImport("user32.dll")]
        public static extern IntPtr SendMessage(IntPtr hWnd, int wMsg, int wParam, int lParam);
        private void pnlControlBar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            WindowInteropHelper helper = new WindowInteropHelper(this);
            SendMessage(helper.Handle, 161, 2, 0);

        }
        private void pnlControlBar_MouseEnter(object sender, MouseEventArgs e)
        {
            this.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }
        private void btnMaximize_Click(object sender, RoutedEventArgs e)
        {
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            this.Report.Visibility = Visibility.Collapsed;
            this.Evaluation.Visibility = Visibility.Collapsed;
            MngProjects.Visibility = Visibility.Collapsed;
            MngGroups.Visibility = Visibility.Collapsed;
            MngAdvisors.Visibility = Visibility.Collapsed;
            MngStudents.Visibility = Visibility.Visible;
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            this.Report.Visibility = Visibility.Collapsed;
            this.Evaluation.Visibility = Visibility.Collapsed;
            MngProjects.Visibility = Visibility.Collapsed;
            MngGroups.Visibility = Visibility.Collapsed;
            MngStudents.Visibility = Visibility.Collapsed;
            MngAdvisors.Visibility = Visibility.Visible;
        }

        private void RadioButton_Checked_2(object sender, RoutedEventArgs e)
        {
            this.Report.Visibility = Visibility.Collapsed;
            this.Evaluation.Visibility = Visibility.Collapsed;
            MngProjects.Visibility = Visibility.Collapsed;
            MngStudents.Visibility = Visibility.Collapsed;
            MngAdvisors.Visibility = Visibility.Collapsed;
            MngGroups.Visibility = Visibility.Visible;
        }

        private void RadioButton_Checked_3(object sender, RoutedEventArgs e)
        {
            this.Report.Visibility = Visibility.Collapsed;
            this.Evaluation.Visibility = Visibility.Collapsed;
            MngStudents.Visibility = Visibility.Collapsed;
            MngAdvisors.Visibility = Visibility.Collapsed;
            MngGroups.Visibility = Visibility.Collapsed;
            MngProjects.Visibility = Visibility.Visible;
        }

        private void RadioButton_Checked_4(object sender, RoutedEventArgs e)
        {
            this.Report.Visibility = Visibility.Collapsed;
            MngStudents.Visibility = Visibility.Collapsed;
            MngAdvisors.Visibility = Visibility.Collapsed;
            MngGroups.Visibility = Visibility.Collapsed;
            MngProjects.Visibility = Visibility.Collapsed;
            this.Evaluation.Visibility = Visibility.Visible;

        }

        private void RadioButton_Checked_5(object sender, RoutedEventArgs e)
        {
            MngStudents.Visibility = Visibility.Collapsed;
            MngAdvisors.Visibility = Visibility.Collapsed;
            MngGroups.Visibility = Visibility.Collapsed;
            MngProjects.Visibility = Visibility.Collapsed;
            this.Evaluation.Visibility = Visibility.Collapsed;
            this.Report.Visibility = Visibility.Visible;
        }
    }
}
