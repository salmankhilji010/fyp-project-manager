﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FYP_project_manager
{
    /// <summary>
    /// Interaction logic for ManageAdvisors.xaml
    /// </summary>
    public partial class ManageAdvisors : UserControl
    {
        public ManageAdvisors()
        {
            InitializeComponent();
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            this.upAdvisor.Visibility = Visibility.Collapsed;
            this.AddAdvisor.Visibility = Visibility.Collapsed;
            this.dataGrid.Visibility = Visibility.Visible;
            string query = "Select * from Advisor join Person on Advisor.ID = Person.ID";
            var con = Configuration.getInstance().getConnection();
            SqlCommand command = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(command);
            DataTable dt = new DataTable();
            da.Fill(dt);
            this.dataGrid.ItemsSource = dt.DefaultView;
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            this.dataGrid.Visibility = Visibility.Collapsed;
            this.upAdvisor.Visibility = Visibility.Collapsed;
            this.AddAdvisor.Visibility = Visibility.Visible;
        }

        private void RadioButton_Checked_2(object sender, RoutedEventArgs e)
        {
            this.dataGrid.Visibility = Visibility.Collapsed;
            this.AddAdvisor.Visibility = Visibility.Collapsed;
            this.upAdvisor.Visibility = Visibility.Visible;
        }

        private void RadioButton_Checked_3(object sender, RoutedEventArgs e)
        {

        }
    }
}
