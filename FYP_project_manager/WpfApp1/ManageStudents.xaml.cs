﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FYP_project_manager
{
    /// <summary>
    /// Interaction logic for ManageStudents.xaml
    /// </summary>
    public partial class ManageStudents : UserControl
    {
        public ManageStudents()
        {
            InitializeComponent();
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            this.upStudents.Visibility = Visibility.Collapsed;
            this.addStudents.Visibility = Visibility.Collapsed;
            this.dataGrid.Visibility = Visibility.Visible;
            string query = "Select * from Student join Person on Student.ID = Person.ID";
            var con = Configuration.getInstance().getConnection();
            SqlCommand command = new SqlCommand(query,con);
            SqlDataAdapter da = new SqlDataAdapter(command);
            DataTable dt = new DataTable();
            da.Fill(dt);
            this.dataGrid.ItemsSource = dt.DefaultView;
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            this.dataGrid.Visibility = Visibility.Collapsed;
            this.delete.Visibility = Visibility.Collapsed;
            this.upStudents.Visibility = Visibility.Collapsed;
            this.addStudents.Visibility = Visibility.Visible;
        }

        private void RadioButton_Checked_2(object sender, RoutedEventArgs e)
        {
            this.delete.Visibility = Visibility.Collapsed;
            this.dataGrid.Visibility = Visibility.Collapsed;
            this.addStudents.Visibility = Visibility.Collapsed;
            this.upStudents.Visibility = Visibility.Visible;
        }

        private void RadioButton_Checked_3(object sender, RoutedEventArgs e)
        {
            this.dataGrid.Visibility = Visibility.Collapsed;
            this.addStudents.Visibility = Visibility.Collapsed;
            this.upStudents.Visibility = Visibility.Collapsed;
            this.delete.Visibility = Visibility.Visible;
        }
    }
}
