﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FYP_project_manager
{
    /// <summary>
    /// Interaction logic for MarkEvaluation.xaml
    /// </summary>
    public partial class MarkEvaluation : UserControl
    {
        public MarkEvaluation()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime date = (DateTime)this.date.SelectedDate.Value;
            string d = date.ToString("yyyy/MM/dd");
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("insert into GroupEvaluation Values(" + int.Parse(this.groupid.Text) + "," + int.Parse(this.evalid.Text) + "," + int.Parse(this.ObtMarks.Text) + ",'" + d + "')",con);
            cmd.ExecuteNonQuery();
            SqlCommand cmd1 = new SqlCommand("Select * from GroupEvaluation", con);
            SqlDataAdapter ad = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            ad.Fill(dt);
            this.dataGrid.ItemsSource = dt.DefaultView;
        }
    }
}
