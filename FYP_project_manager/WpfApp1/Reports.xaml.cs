﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using MessageBox = System.Windows.Forms.MessageBox;
using System.Data;
using Document = iTextSharp.text.Document;
using System.Data.SqlClient;

namespace FYP_project_manager
{
    /// <summary>
    /// Interaction logic for Reports.xaml
    /// </summary>
    public partial class Reports : System.Windows.Controls.UserControl
    {
        public Reports()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Document document = new Document();
            PdfWriter.GetInstance(document, new FileStream("ProjectReport.pdf", FileMode.Create));

            // Open the PDF document
            document.Open();

            // Adding the heading
            Chunk heading = new Chunk("Projects Report", new Font(Font.FontFamily.HELVETICA));
            iTextSharp.text.Paragraph headingParagraph = new iTextSharp.text.Paragraph(heading);
            headingParagraph.Alignment = Element.ALIGN_CENTER;
            document.Add(headingParagraph);

            string paragraphText = "\nThis is Project Report\n\n";
            iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph(paragraphText);
            paragraph.Font = new Font(Font.FontFamily.HELVETICA);
            document.Add(paragraph);

            // Get Table from SQL Server using Required Query
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * From Project; ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            this.dataGrid.ItemsSource = dt.DefaultView;

            // Create a new table
            PdfPTable table = new PdfPTable(dt.Columns.Count);

            // Add table columns with the font
            foreach (DataColumn column in dt.Columns)
            {
                // Add the phrase to the cell and set background color
                PdfPCell cell = new PdfPCell(new Phrase(column.ColumnName));
                table.AddCell(cell);
            }

            // Add table rows with the font
            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    var cell = new PdfPCell(new Phrase(row[column].ToString()));
                    table.AddCell(cell);
                }
            }

            // Add the table to the PDF document
            document.Add(table);

            // Close the PDF document
            document.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Document document = new Document();
            PdfWriter.GetInstance(document, new FileStream("GroupAndStudentReport.pdf", FileMode.Create));

            // Open the PDF document
            document.Open();

            // Adding the heading
            Chunk heading = new Chunk("Group And Student Report", new Font(Font.FontFamily.HELVETICA));
            iTextSharp.text.Paragraph headingParagraph = new iTextSharp.text.Paragraph(heading);
            headingParagraph.Alignment = Element.ALIGN_CENTER;
            document.Add(headingParagraph);

            string paragraphText = "\nThis is Group And Student Report\n\n";
            iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph(paragraphText);
            paragraph.Font = new Font(Font.FontFamily.HELVETICA);
            document.Add(paragraph);

            // Get Table from SQL Server using Required Query
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * From GroupStudent; ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            this.dataGrid.ItemsSource = dt.DefaultView;

            // Create a new table
            PdfPTable table = new PdfPTable(dt.Columns.Count);

            // Add table columns with the font
            foreach (DataColumn column in dt.Columns)
            {
                // Add the phrase to the cell and set background color
                PdfPCell cell = new PdfPCell(new Phrase(column.ColumnName));
                table.AddCell(cell);
            }

            // Add table rows with the font
            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    var cell = new PdfPCell(new Phrase(row[column].ToString()));
                    table.AddCell(cell);
                }
            }

            // Add the table to the PDF document
            document.Add(table);

            // Close the PDF document
            document.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Document document = new Document();
            PdfWriter.GetInstance(document, new FileStream("GroupAndProjectWithAdvisoryBoardReport.pdf", FileMode.Create));

            // Open the PDF document
            document.Open();

            // Adding the heading
            Chunk heading = new Chunk("GroupAndProjectWithAdvisoryBoard Report", new Font(Font.FontFamily.HELVETICA));
            iTextSharp.text.Paragraph headingParagraph = new iTextSharp.text.Paragraph(heading);
            headingParagraph.Alignment = Element.ALIGN_CENTER;
            document.Add(headingParagraph);

            string paragraphText = "\nThis is GroupAndProjectWithAdvisoryBoardject Report\n\n";
            iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph(paragraphText);
            paragraph.Font = new Font(Font.FontFamily.HELVETICA);
            document.Add(paragraph);

            // Get Table from SQL Server using Required Query
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * From GroupProject join ProjectAdvisor on GroupProject.Projectid = ProjectAdvisor.Projectid; ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            this.dataGrid.ItemsSource = dt.DefaultView;

            // Create a new table
            PdfPTable table = new PdfPTable(dt.Columns.Count);

            // Add table columns with the font
            foreach (DataColumn column in dt.Columns)
            {
                // Add the phrase to the cell and set background color
                PdfPCell cell = new PdfPCell(new Phrase(column.ColumnName));
                table.AddCell(cell);
            }

            // Add table rows with the font
            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    var cell = new PdfPCell(new Phrase(row[column].ToString()));
                    table.AddCell(cell);
                }
            }

            // Add the table to the PDF document
            document.Add(table);

            // Close the PDF document
            document.Close();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Document document = new Document();
            PdfWriter.GetInstance(document, new FileStream("Student.pdf", FileMode.Create));

            // Open the PDF document
            document.Open();

            // Adding the heading
            Chunk heading = new Chunk("Student Report", new Font(Font.FontFamily.HELVETICA));
            iTextSharp.text.Paragraph headingParagraph = new iTextSharp.text.Paragraph(heading);
            headingParagraph.Alignment = Element.ALIGN_CENTER;
            document.Add(headingParagraph);

            string paragraphText = "\nThis is Student Report\n\n";
            iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph(paragraphText);
            paragraph.Font = new Font(Font.FontFamily.HELVETICA);
            document.Add(paragraph);

            // Get Table from SQL Server using Required Query
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * From Person p join Student S on S.id = p.id; ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            this.dataGrid.ItemsSource = dt.DefaultView;

            // Create a new table
            PdfPTable table = new PdfPTable(dt.Columns.Count);

            // Add table columns with the font
            foreach (DataColumn column in dt.Columns)
            {
                // Add the phrase to the cell and set background color
                PdfPCell cell = new PdfPCell(new Phrase(column.ColumnName));
                table.AddCell(cell);
            }

            // Add table rows with the font
            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    var cell = new PdfPCell(new Phrase(row[column].ToString()));
                    table.AddCell(cell);
                }
            }

            // Add the table to the PDF document
            document.Add(table);

            // Close the PDF document
            document.Close();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            Document document = new Document();
            PdfWriter.GetInstance(document, new FileStream("Mark sheet Report.pdf", FileMode.Create));

            // Open the PDF document
            document.Open();

            // Adding the heading
            Chunk heading = new Chunk("MarkSheet Report", new Font(Font.FontFamily.HELVETICA));
            iTextSharp.text.Paragraph headingParagraph = new iTextSharp.text.Paragraph(heading);
            headingParagraph.Alignment = Element.ALIGN_CENTER;
            document.Add(headingParagraph);

            string paragraphText = "\nThis is MarkSheet Report\n\n";
            iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph(paragraphText);
            paragraph.Font = new Font(Font.FontFamily.HELVETICA);
            document.Add(paragraph);

            // Get Table from SQL Server using Required Query
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT * From Student S join GroupStudent G on S.id = G.StudentId join GroupEvaluation GE on GE.Groupid = G.GroupId; ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
                        this.dataGrid.ItemsSource = dt.DefaultView;

            // Create a new table
            PdfPTable table = new PdfPTable(dt.Columns.Count);

            // Add table columns with the font
            foreach (DataColumn column in dt.Columns)
            {
                // Add the phrase to the cell and set background color
                PdfPCell cell = new PdfPCell(new Phrase(column.ColumnName));
                table.AddCell(cell);
            }

            // Add table rows with the font
            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    var cell = new PdfPCell(new Phrase(row[column].ToString()));
                    table.AddCell(cell);
                }
            }

            // Add the table to the PDF document
            document.Add(table);

            // Close the PDF document
            document.Close();
        }

        }
}
