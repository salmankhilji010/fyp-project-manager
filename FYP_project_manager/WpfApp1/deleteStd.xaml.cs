﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FYP_project_manager
{
    /// <summary>
    /// Interaction logic for deleteStd.xaml
    /// </summary>
    public partial class deleteStd : UserControl
    {
        public deleteStd()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<string> listid = new List<string>();
                string id = this.id.Text;
                if (id == "")
                {
                    MessageBox.Show("Please enter something.");
                }
                else
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Delete from Student where Id = " + id, con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd1 = new SqlCommand("Delete from Person where Id = " + id, con);
                    cmd1.ExecuteNonQuery();

                    int ID = 0;
                    SqlCommand cmd2 = new SqlCommand("Select Id from Student where Id = " + id, con);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        listid.Add(reader.GetString(0));
                    }
                    if (listid.Count > 0)
                    {
                        if (listid[0] == id)
                        {
                            MessageBox.Show("Record has been deleted.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Record has not been found.");
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Record has not been found.");
            }
        }
    }
}