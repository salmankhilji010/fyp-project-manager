﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FYP_project_manager
{
    /// <summary>
    /// Interaction logic for updateStudent.xaml
    /// </summary>
    public partial class updateStudent : UserControl
    {
        public int count = 0;
        public updateStudent()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string id = this.id.Text;
                string gender = "";
                string regno = this.RegNo.Text;
                string fname = this.FName.Text;
                string lname = this.LName.Text;
                string email = this.Email.Text;
                string contact = this.Contact.Text;
                DateTime date = (DateTime)this.date.SelectedDate.Value;
                string d = date.ToString("yyyy/MM/dd");
                if (this.ComboBox1.SelectedIndex == 0)
                {
                    gender = "Male";
                }
                else
                {
                    gender = "Female";
                }
                if (regno == "" || fname == "" || lname == "" || email == "" || contact == "" || d == null || gender == null)
                {
                    MessageBox.Show("Please Enter data in all the fields.");
                    return;
                }
                else
                {
                    int i = 0; bool flag = true;
                    while (i < email.Length)
                    {

                        if (email[i] == '@')
                        {
                            flag = false;
                        }
                        i++;
                    }
                    if (flag == true)
                    {
                        MessageBox.Show("Enter a valid Email Address");
                        return;
                    }
                    
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd0 = new SqlCommand("Select ID from lookup where Value = '" + gender + "'", con);
                    int ID = (int)cmd0.ExecuteScalar();
                    SqlCommand cmd = new SqlCommand("Update Student Set RegistrationNo = '" + regno + "' where Id = " + id, con);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd1 = new SqlCommand("Update Person Set FirstName ='" + fname + "', LastName = '" + lname + "', Contact = '" + contact + "', Email = '" + email + "', DateOfBirth = '" + d + "', Gender = '" + ID + "' where Id = '" + id + "'", con);
                    cmd1.ExecuteNonQuery();
                    MessageBox.Show("Record has been updated.");
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.ToString());
            }
        }

        private void ComboBox1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (count == 0)
            {
                count++;
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select Value from lookup where Category = 'GENDER'", con);

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string item = reader.GetString(0);
                    this.ComboBox1.Items.Add(item);
                    Console.WriteLine(item);
                }

                reader.Close();
            }
        }
    }
}
